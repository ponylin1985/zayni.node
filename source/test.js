const readLine   = require( 'readline' );
const colors     = require( 'colors' );
const zayni      = require( './common.js' );
const extentions = require( './extensions.js' );
const jsonUtil   = require( './json.js' );
const logger     = require( './log.js' );

const common = zayni.common;
const guid   = zayni.guid;

logger.useConfig( {
    enableConsoleLog : true,
    enableFileLog    : true,
    fileLogPath      : __dirname + '/LogFile/zayni.logger.test.log',
    logFileMaxSize   : 2
} );

let obj2 = {
    Name     : 'Joe Henderson',
    Age      : 56,
    sex      : 'male',
    IsMaster : true
};

let j = jsonUtil.serialize( obj2, 4 );
console.log( j );

// console.info( 'Hi this is zayni.node framework.', colors.green );
// console.info( 'Test {userId}.'.format( { userId : 'Pony123' } ), colors.green );

// let msg = '';
// common.isNullOrEmptyThen( msg, () => console.log( 'cool', { color : colors.green } ) );
// console.log( guid.createGuid() );

// for ( let i = 0; i < 10; i++ ) {
//     console.log( guid.createGuid() );
// }

// let test = 'abc';
// common.isTrue( 'abc' === test, () => console.log( 'test is abc' ) );
// common.isFalse( 123 === test, () => console.log( 'test is not 123' ) );

// let source = 'Fuck you, Suck my dick...';
// let result = source.removeFirstAppeared( 'Fuck you, ' );
// console.log( result, { color : colors.america } );

// console.log( `Current timestamp is ${common.getCurrentTimestamp()}` );
// console.log( `Current timestamp is ${common.getCurrentTimestampMillisecond()}` );

// for ( let i = 0; i < 30; i++ ) {
//     console.log( common.createRandomText( 12 ) );
// }

const ServiceHost   = require( './serviceHost/service.js' );
const ServiceAction = require( './serviceHost/service-action.js' );
const container     = require( './serviceHost/service-action-container.js' );

class MyAction extends ServiceAction {
    constructor() {
        super( 'Test' );
    }

    execute() {
        return new Promise( ( resolve, reject ) => {
            setTimeout( () => {
                let request = this.requestData;
                console.log( `I got your user data request. UserId is ${request.userId}.` );
                
                let somethings = [];

                // Socket Stream 大型資料的測試
                for ( let i = 0; i < 5000; i++ ) {
                    let some = {
                        srNo : i + 1,
                        msg  : 'ddee'
                    };

                    somethings.push( some );
                }

                this.result = { 
                    isSuccess : true,
                    code : '32',
                    data : {
                        userId     : request.userId,
                        userName   : request.userName + ' Ha Ha Ha',
                        birthday   : new Date( '1985-10-24' ),
                        girlFriend : [
                            {
                                name    : 'Amber',
                                special : [ 'Angl Fuck', 'Deep Thoat Fuck', '3P Fuck', 'Drink Sperm' ],
                                age     : 21,
                                isSlut  : true
                            },
                            {
                                name    : 'Cindy',
                                special : [ 'Angl Fuck', 'Deep Thoat Fuck', '3P Fuck' ],
                                age     : 24,
                                isBitch : true
                            }
                        ],
                        somethings : somethings
                    },
                    message : 'ok'
                };

                return resolve();
            }, 300 );
        } );
    }
}

class SomeAction extends ServiceAction {
    constructor() {
        super( 'Something' );
    }

    execute() {
        return new Promise( ( resolve, reject ) => {
            setTimeout( () => {
                let request = this.requestData;
                console.log( `I got your user data request. UserId is ${request.userId}.` );

                this.result = { 
                    isSuccess : true,
                    code : '32',
                    data : {
                        userId     : request.userId,
                        userName   : request.userName + ' Ha Ha Ha',
                        birthday   : new Date( '1985-10-24' ),
                        girlFriend : [
                            {
                                name    : 'Amber',
                                special : [ 'Angl Fuck', 'Deep Thoat Fuck', '3P Fuck', 'Drink Sperm' ],
                                age     : 21,
                                isSlut  : true
                            },
                            {
                                name    : 'Cindy',
                                special : [ 'Angl Fuck', 'Deep Thoat Fuck', '3P Fuck' ],
                                age     : 24,
                                isBitch : true
                            }
                        ]
                    },
                    message : 'ok'
                };

                return resolve();
            }, 500 );
        } );
    }
}

// let a = new MyAction();
// console.log( a.constructor );
// let obj = Object.create( a.constructor.prototype );
// console.log( obj );
// obj.requestData = { "ddd": "eee" };
// obj.execute();

container.addServiceAction( new MyAction() );
container.addServiceAction( new SomeAction() );

let host = new ServiceHost();
host.startHost( '127.0.0.1', 889 );

let rl = readLine.createInterface( {
    input  : process.stdin,
    output : process.stdout	
} );

rl.on( 'line', function ( command ) {
    if ( 'send' === command ) {
        let data = {
            userId   : 'pony001',
            userName : 'Pony Lin',
            sex      : 1,
            age      : 31
        };

        host.rpc( 'cc', data, 3 )
            .then( responsePackage => {
                let json = jsonUtil.serialize( responsePackage );
                console.log( 'RPC response:' );
                console.log( `requestId: ${responsePackage.requestId}` );
                console.log( `isSuccess: ${responsePackage.isSuccess}` );
                console.log( json );
            } )
            .catch( error => {
                console.error( `RPC error: ${error}` );
            } );
    }
} );