'use strict';

const colors         = require( 'colors' );
const net            = require( 'net' );
const zayni          = require( '../common.js' );
const extensions     = require( '../extensions.js' );
const jsonUtil       = require( '../json.js' );
const Result         = require( '../entity' ).Result;
const SocketEvent    = require( './socket-event.js' );
const RequestPackage = require( './entity/request-package.js' );
const common         = zayni.common;
const guid           = zayni.guid;

/** Socket Host Server 服務物件
 * @description Socket Host Server 服務物件
 * @type {Server}
 */
let _server = null;

/** Socket 連線池
 * @description 目前所有的 Socket 連線集合
 * @type {Array}
 */
let _connections = [];

/** 請求包裹集合
 * @description 請求包裹集合
 * @type {Array}
 */
const _requests = require( './requests.js' );

/** 開啟 Socket Host Service 服務
 * @description 開啟 Socket Host Service 服務
 * @param {Socket} socket TCP Socket 物件
 * @returns {Connection} Socket 連線資訊。
 */
function startService( socket ) {
    socket.connectionId = createConnectionId();
    socket.clientHost   = socket.remoteAddress;
    socket.clientPort   = socket.remotePort;
    _connections.push( socket );
    
    let socketEventHandler = new SocketEvent( socket );
    console.log( `Socket connection established success. Socket connectionId: ${socket.connectionId}.` );

    socket.setNoDelay( true );
    socket.setEncoding( 'utf8' );

    socket.on( 'data', data => {
        console.log( `Data event fire, connectionId: ${socket.connectionId}.` );
        socketEventHandler.data( data );
    } );
    
    socket.on( 'error', ex => {
        socketEventHandler.error( ex );
    } );
    
    socket.on( 'end', fin => {
        socketEventHandler.end( fin );
    } );
    
    socket.on( 'close', () => {
        let conn = _connections.where( g => g.connectionId === socket.connectionId ).firstOrDefault();
        _connections.remove( conn );
        socketEventHandler.disconnect();
    } );

    return socket;
}

/** 建立連線識別代碼
 * @description 建立連線識別代碼
 * @returns {String} Socket 連線識別代碼
 */
function createConnectionId() {
    let connectionId = common.createRandomText( 12 );
    let connections  = _connections.select( m => { connectionId : m.connectionId } );

    if ( connections.contains( connectionId ) ) {
        return createConnectionId();
    }

    return connectionId;
}

/** 服務託管
 * @description Self-host 服務託管類別
 * @class ServiceHost
 */
class ServiceHost {

    /** 開啟 Service Host 服務
     * @description 開啟 Service Host 服務
     * @param {String} host 服務端的主機名稱或IP位址
     * @param {Number} port 服務端監聽的連接阜號
     * @returns {Promise} 執行結果 Promise 物件
     * @memberOf ServiceHost
     */
    startHost( host, port ) {
        return new Promise( ( resolve, reject ) => {
            let result     = new Result( false );
            let connection = null;

            _server = net.createServer( socket => {
                connection = startService( socket );
            } );

            _server.listen( port, host, function ( error ) {
                if ( error ) {
                    console.error( `Start service RPC host occur exception: ${error}` );
                    return reject( error );
                }

                result.data      = connection;
                result.isSuccess = true;
                return resolve( result );
            } );
        } );
    }

    /** 執行 RPC 遠端動作呼叫
     * @description 執行 RPC 遠端動作呼叫，執行客戶端的動作。
     * @param {String} actionName 動作名稱。
     * @param {any} data 請求的資料集合。
     * @param {Number} timeout 等待 RPC 回應逾時秒數。
     * @returns {Promise} 回應包裹集合的 Promise 物件
     */
    rpc( actionName, data, timeout ) {
        return new Promise( ( resolve, reject ) => {
            timeout = timeout || 30;

            if ( common.isNullOrEmpty( actionName ) ) {
                return reject( new Error( `Argument 'actionName' can not be null or empty string.` ) );
            }

            let requestPackage         = new RequestPackage();
            requestPackage.requestId   = guid.createGuid();
            requestPackage.messageType = 'RpcRequest';
            requestPackage.actionName  = actionName;
            requestPackage.data        = data;
            requestPackage.requestTime = new Date();
            requestPackage.timeoutTime = new Date( requestPackage.requestTime.setSeconds( requestPackage.requestTime.getSeconds() + timeout ) );

            let json        = jsonUtil.serialize( requestPackage );
            let jsonMessage = JSON.stringify( requestPackage ) + '\n';
            let socket      = _connections.firstOrDefault();     // Pony TODO: 這邊還要調整，目前只抓第一個 Socket Connection Client 進行 RPC 請求發送...
            console.log( `Sending outgoing socket message: \n${json}`, { color : colors.magenta } );
            socket.write( new Buffer( jsonMessage ) );

            let interval = setInterval( () => {
                let r = _requests.where( q => q.requestId === requestPackage.requestId );

                if ( common.isNullOrEmpty( r ) || 0 === r.length ) {
                    return;
                }

                let req = r.firstOrDefault();

                if ( common.isNullOrEmpty( req ) ) {
                    return reject( new Error( `Can not get original RPC request from request pool. RequestId is ${requestPackage.requestId}.` ) );
                }

                let nowTime = new Date();

                if ( nowTime > req.timeoutTime ) {
                    clearInterval( req.interval );
                    _requests.remove( req );
                    return reject( new Error( `RPC response is timeout. RequestId is ${req.requestId}.` ) );
                }

                let responsePackage = req.response;

                if ( common.isNullOrEmpty( responsePackage ) ) {
                    return;
                }
                
                clearInterval( req.interval );
                _requests.remove( req );
                return resolve( responsePackage );
            }, 2 );

            requestPackage.interval = interval;
            _requests.push( requestPackage );
        } );
    }
}

module.exports = ServiceHost;