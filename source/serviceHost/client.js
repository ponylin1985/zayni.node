'use strict';

const net             = require( 'net' );
const colors          = require( 'colors' );
const RequestPackage  = require( './entity/request-package.js' );
const ResponsePackage = require( './entity/response-package.js' );
const actionContainer = require( './service-action-container.js' );
const zayni           = require( '../common.js' );
const ext             = require( '../extensions.js' );
const jsonUtil        = require( '../json.js' );
const common          = zayni.common;
const guid            = zayni.guid;
const Socket          = net.Socket;

/** 請求包裹集合
 * @description 請求包裹集合
 * @type {Array}
 */
const _requests = [];

/** Socket 訊息分隔字元
 * @description Socket 訊息分隔字元
 * @type {String}
 */
let _delimiter = '\n';

/** Socket 連線是否正常建立
 * @description Socket 連線是否正常建立
 * @type {Boolean}
 */
let _isConnected = false;

/** 服務的客戶端元件類別
 * @description 服務的客戶端元件類別
 * @class ServiceClient
 */
class ServiceClient {

    /** 建構子函式
     * @description 建立 ServiceClient 物件
     * @memberOf ServiceClient
     */
    constructor() {

        /** Socket 訊息緩存字串
         * @description Socket 訊息緩存字串
         * @type {String}
         */
        this.msgBuffers = '';

        /** TCP Socket 物件
         * @description TCP Socket 物件
         * @type {Socket}
         */
        this.socket = new Socket( { readable : true, writable : true } );
    }

    /** 連線至遠端服務
     * @description 連線至遠端服務
     * @param {String} host 服務端的主機名稱或 IP 位址
     * @param {Number} port 服務端的連接阜號
     */
    connect( host, port ) {
        connect( host, port, this.socket, this.msgBuffers );
    }

    /** 執行 RPC 遠端服務呼叫
     * @description 執行 RPC 遠端服務呼叫
     * @param {String} actionName 動作名稱。
     * @param {any} data 請求的資料集合。
     * @param {Number} timeout 等待 RPC 回應逾時秒數。
     * @returns {Promise} 回應包裹集合的 Promise 物件
     */
    rpc( actionName, data, timeout ) {
        return new Promise( ( resolve, reject ) => {
            timeout = timeout || 30;

            if ( common.isNullOrEmpty( actionName ) ) {
                return reject( new Error( `Argument 'actionName' can not be null or empty string.` ) );
            }

            let requestPackage         = new RequestPackage();
            requestPackage.requestId   = guid.createGuid();
            requestPackage.messageType = 'RpcRequest';
            requestPackage.actionName  = actionName;
            requestPackage.data        = data;
            requestPackage.requestTime = new Date();
            requestPackage.timeoutTime = new Date( requestPackage.requestTime.setSeconds( requestPackage.requestTime.getSeconds() + timeout ) );

            sendSocketMessage( this.socket, requestPackage );
            _requests.push( requestPackage );

            let f = setInterval( () => {
                let r = _requests.where( q => q.requestId === requestPackage.requestId );

                if ( common.isNullOrEmpty( r ) || 0 === r.length ) {
                    return;
                }

                let req = r.firstOrDefault();

                if ( common.isNullOrEmpty( req ) ) {
                    clearInterval( f );
                    return reject( new Error( `Can not get original RPC request from request pool. RequestId is ${requestPackage.requestId}.` ) );
                }

                let nowTime = new Date();

                if ( nowTime > req.timeoutTime ) {
                    clearInterval( f );
                    _requests.remove( req );
                    return reject( new Error( `RPC response is timeout. RequestId is ${req.requestId}.` ) );
                }

                let responsePackage = req.response;

                if ( common.isNullOrEmpty( responsePackage ) ) {
                    return;
                }
                
                clearInterval( f );
                _requests.remove( req );
                return resolve( responsePackage );
            }, 2 );
        } );
    }
}

/** 連線至遠端服務
 * @description 連線至遠端服務
 * @param {String} host 服務端的主機名稱或IP位址
 * @param {Number} port 服務端的連接阜號
 * @param {Socket} socket TCP Socket 物件
 * @param {any} msgBuffers TCP Socket 訊息緩存字串
 */
function connect( host, port, socket, msgBuffers ) {
    socket.connect( port, host, function ( error ) {
        console.log( 'Connect to remote service success.' );

        /** 接收到遠端服務的訊息資料的處理
         * @description 接收到遠端服務的訊息資料
         */
        socket.on( 'data', function ( data ) {
            try {
                msgBuffers += data;
                let index   = msgBuffers.indexOf( _delimiter );

                while ( index > -1 ) {
                    let json = msgBuffers.substring( 0, index );
                    setImmediate( () => receive( socket, json ) );
                    msgBuffers = msgBuffers.substring( index + 1 );
                    index      = msgBuffers.indexOf( _delimiter );
                }
            }
            catch ( ex ) {
                console.error( `Receiving tcp socket data and process occur exception: ${ex.message}` );
                console.error( `Receive raw data:\n${data}` );
                return;
            }
        } );

        /** Socket 連線發生異常的處理
         * @description Socket 連線發生異常的處理
         */
        socket.on( 'error', function ( ex ) {
            console.error( `Socket connection occur exception: ${ex.message}` );
        } );

        /** Socket 連線關閉的處理
         * @description Socket 連線關閉的處理
         */
        socket.on( 'close', function () {
            console.log( `Socket connection is closed.` );
            _isConnected = false;

            let r = setInterval( function () {
                if ( _isConnected ) {
                    clearInterval( r );
                    return;
                }

                connect( host, port, socket, msgBuffers );
                _isConnected = true;
            }, 1000 * 5 );
        } );

        _isConnected = true;
    } );
}

/** 接收到 Socket 資料訊息的處理
 * @description 接收到 Socket 資料訊息的處理
 * @param {Socket} socket TCP Socket 連線物件
 * @param {any} data Socket 資料訊息
 */
function receive( socket, data ) {
    let responsePackage = jsonUtil.deserialize( data.trim() );
    let json            = jsonUtil.serialize( responsePackage );
    console.log( `Receive incoming socket message: \n${json}`, { color : colors.green } );

    switch ( responsePackage.messageType ) {
        case 'RpcResponse':
            rpcResponseProcess( responsePackage );
            return;
        
        case 'RpcRequest':
            rpcRequestProcess( socket, responsePackage );
            return;
    }
}

/** 處理 RPC 回應訊息
 * @description 處理 RPC 回應訊息
 * @param {ResponsePackage} responsePackage 服務回應訊息包裹
 */
function rpcResponseProcess( responsePackage ) {
    setImmediate( () => {
        let requestId = null;

        try {
            requestId = responsePackage.requestId;
        }
        catch ( ex ) {
            console.error( `Service client try get requestId from response package occur exception. \n${ex.message}` );
            return;
        }

        if ( common.isNullOrEmpty( requestId ) ) {
            console.error( `Service client can not get requestId from response package.` );
            return;
        }
        
        let r = _requests.where( r => r.requestId === requestId );

        if ( 0 === r.length || common.isNullOrEmpty( r ) ) {
            console.warn( `Service client receiving service RPC response data but can not mapping requestId. Possible request '${requestId}' is timeout.` );
            return;
        }

        let request = r.firstOrDefault();

        if ( common.isNullOrEmpty( request ) ) {
            return;
        }

        try {
            request.response = responsePackage;
        }
        catch ( ex ) {
            console.error( `Set the RPC response package to RPC request occur exception. \n${ex.message}` );
            return;
        }
    } );
}

/** 處理 RPC 請求訊息
 * @description 處理 RPC 請求訊息
 * @param {Socket} socket TCP Socket 連線物件
 * @param {RequestPackage} requestPackage 服務請求訊息包裹
 */
function rpcRequestProcess( socket, requestPackage ) {
    let responsePackage         = new ResponsePackage();
    responsePackage.requestId   = requestPackage.requestId;
    responsePackage.messageType = 'RpcResponse';
    responsePackage.actionName  = requestPackage.actionName;

    let actionName = null;

    if ( !requestPackage.hasOwnProperty( 'actionName' ) ) {
        console.error( `Receiving data doesn't contains 'actionName' property.` );
        return;
    }

    try {
        actionName = requestPackage.actionName;
    }
    catch ( ex ) {
        console.error( `Get receive 'actionName' occur exception: ${ex.message}` );
        return;
    }

    if ( common.isNullOrEmpty( actionName ) ) {
        console.error( `Receive 'actionName' from client side is null or empty.` );
        return;
    }

    let action = null;

    try {
        action = actionContainer.getServiceAction( actionName );
    }
    catch ( ex ) {
        console.error( `Get servcie action occur exception. Action name is ${actionName}. \n${ex}` );
        return;
    }

    if ( common.isNullOrEmpty( action ) ) {
        console.error( `ServiceAction instance reciving null from service action container. Action name is ${actionName}.` );
        return;
    }
    
    action.requestData = requestPackage.data;

    action
        .execute()
        .then( () => {
            let actionResult = action.result;

            if ( common.isNullOrEmpty( actionResult ) ) {
                console.error( `ActionResult is null from service action. Action name is ${actionName}.` );
                return;
            }

            responsePackage.data      = actionResult.data;
            responsePackage.code      = actionResult.code;
            responsePackage.message   = actionResult.message;
            responsePackage.isSuccess = actionResult.isSuccess;
            sendSocketMessage( socket, responsePackage );
        } )
        .catch( error => {
            console.error( `Execute service action occur exception. Action name is ${actionName}. \n${error}` );

            responsePackage.exception = error;
            responsePackage.code      = 9988;
            responsePackage.message   = error.message;
            responsePackage.isSuccess = false;
            sendSocketMessage( socket, responsePackage );
        } );
}

/** 發送 Socket 連線訊息
 * @description 發送 Socket 連線訊息
 * @param {Socket} socket TCP Socket 連線物件
 * @param {ResponsePackage} message Socket 訊息物件
 */
function sendSocketMessage( socket, message ) {
    let json        = jsonUtil.serialize( message );
    let jsonMessage = JSON.stringify( message ) + '\n';
    let rpcMessage  = new Buffer( jsonMessage );

    console.log( `Sending outgoing socket message: \n${json}`, { color : colors.magenta } );

    try {
        socket.write( rpcMessage );
    }
    catch ( ex ) {
        console.error( `Sending socket message occur exception. Socket connectionId is ${socket.connectionId}. \n${ex.message}` );
    }
}

module.exports = ServiceClient;