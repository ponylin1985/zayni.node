'use strict';

const colors                 = require( 'colors' );
const serviceActionContainer = require( './service-action-container.js' );
const ResponsePackage        = require( './entity/response-package.js' );
const common                 = require( '../common' ).common;
const jsonUtil               = require( '../json/json-serialization.js' );

/** 請求包裹集合
 * @description 請求包裹集合
 * @type {Array}
 */
const _requests = require( './requests.js' );

/** Socket 訊息分隔字元
 * @description Socket 訊息分隔字元
 * @type {String}
 */
let _delimiter = '\n';

/** Socket 事件處理器
 * @description Socket 事件處理器
 * @class SocketEventHandler
 */
class SocketEventHandler {

    /** 建構子函式
     * @description 建立 SocketEventHandler 物件
     * @param {Socket} socket TCP Socket 連線物件
     * @memberOf SocketEventHandler
     */
    constructor( socket ) {

        /** Socket 訊息緩存字串
         * @description Socket 訊息緩存字串
         * @type {String}
         */
        this.msgBuffers = '';

        /** TCP Socket 物件
         * @description TCP Socket 物件
         * @type {Socket}
         */
        this.socket = socket;
    }

    /** Socket 連線接收到訊息資料的事件處理
     * @description 接收到訊息資料的事件處理
     * @param {any} data 資料集合
     * @memberOf SocketEventHandler
     */
    data( data ) {
        try {
            let socket     = this.socket;
            let msgBuffers = this.msgBuffers;

            msgBuffers += data;
            let index   = msgBuffers.indexOf( _delimiter );

            while ( index > -1 ) {
                let json = msgBuffers.substring( 0, index );
                setImmediate( () => process( socket, json ) );
                msgBuffers = msgBuffers.substring( index + 1 );
                index      = msgBuffers.indexOf( _delimiter );
            }
        }
        catch ( ex ) {
            console.error( `Receiving tcp socket data and process occur exception: ${ex.message}` );
            console.error( `Receive raw data:\n${data}` );
            return;
        }
    }

    /** Socket 連線發生程式異常的處理
     * @description Socket 連線發生程式異常的處理
     * @param {Error} ex 程式異常物件
     * @memberOf SocketEventHandler
     */
    error( ex ) {
        console.error( `Socket connection occur exception. Socket connectionId is ${this.socket.connectionId}. \n${ex}` );
    }

    /** Socket 連線接收到 FIN data 的處理
     * @description Socket 連線接收到 FIN data 的處理
     * @param {any} fin FIN data
     * @memberOf SocketEventHandler
     */
    end( fin ) {
        console.log( `Receive FIN data. Socket connectionId is ${this.socket.connectionId}.` );
    }

    /** Socket 連線串流關閉的處理
     * @description Socket 連線串流關閉的處理
     * @memberOf SocketEventHandler
     */
    disconnect() {
        console.log( `Socket connection is closed. Socket connectionId is ${this.socket.connectionId}.` );
    }
}

/** RPC 遠端程序請求呼叫的處理
 * @description RPC 遠端程序請求呼叫的處理
 * @param {Socket} socket TCP Socket 連線物件
 * @param {any} data Socket 資料訊息
 */
function process( socket, data ) {
    let requestPackage = jsonUtil.deserialize( data.trim() );
    let jsonRequest    = jsonUtil.serialize( requestPackage );
    console.log( `Receive incoming socket message: \n${jsonRequest}`, { color : colors.green } );

    switch ( requestPackage.messageType ) {
        case 'RpcRequest':
            processRpcRequest( socket, requestPackage );
            return;
    
        case 'RpcResponse':
            processRpcResponse( requestPackage );
            return;
    }
}

/** 處理 RPC 請求訊息
 * @description 處理 RPC 請求訊息
 * @param {Socket} socket TCP Socket 連線物件
 * @param {RequestPackage} requestPackage 服務請求訊息包裹
 */
function processRpcRequest( socket, requestPackage ) {
    let responsePackage         = new ResponsePackage();
    responsePackage.requestId   = requestPackage.requestId;
    responsePackage.messageType = 'RpcResponse';
    responsePackage.actionName  = requestPackage.actionName;

    let actionName = null;

    if ( !requestPackage.hasOwnProperty( 'actionName' ) ) {
        console.error( `Receiving data doesn't contains 'actionName' property.` );
        return;
    }

    try {
        actionName = requestPackage.actionName;
    }
    catch ( ex ) {
        console.error( `Get receive 'actionName' occur exception: ${ex.message}` );
        return;
    }

    if ( common.isNullOrEmpty( actionName ) ) {
        console.error( `Receive 'actionName' from client side is null or empty.` );
        return;
    }

    let action = null;

    try {
        action = serviceActionContainer.getServiceAction( actionName );
    }
    catch ( ex ) {
        console.error( `Get servcie action occur exception. Action name is ${actionName}. \n${ex}` );
        return;
    }

    if ( common.isNullOrEmpty( action ) ) {
        console.error( `ServiceAction instance reciving null from service action container. Action name is ${actionName}.` );
        return;
    }
    
    action.requestData = requestPackage.data;

    action
        .execute()
        .then( () => {
            let actionResult = action.result;

            if ( common.isNullOrEmpty( actionResult ) ) {
                console.error( `ActionResult is null from service action. Action name is ${actionName}.` );
                return;
            }

            responsePackage.data      = actionResult.data;
            responsePackage.code      = actionResult.code;
            responsePackage.message   = actionResult.message;
            responsePackage.isSuccess = actionResult.isSuccess;
            sendSocketMessage( socket, responsePackage );
        } )
        .catch( error => {
            console.error( `Execute service action occur exception. Action name is ${actionName}. \n${ex}` );

            responsePackage.exception = error;
            responsePackage.code      = 9988;
            responsePackage.message   = error.message;
            responsePackage.isSuccess = false;
            sendSocketMessage( socket, responsePackage );
        } );
}

/** 處理 RPC 回應訊息
 * @description 處理 RPC 回應訊息
 * @param {ResponsePackage} responsePackage 服務回應訊息包裹
 */
function processRpcResponse( responsePackage ) {
    setImmediate( () => {
        let requestId = null;

        try {
            requestId = responsePackage.requestId;
        }
        catch ( ex ) {
            console.error( `Service client try get requestId from response package occur exception. \n${ex.message}` );
            return;
        }

        if ( common.isNullOrEmpty( requestId ) ) {
            console.error( `Service client can not get requestId from response package.` );
            return;
        }
        
        let r = _requests.where( r => r.requestId === requestId );

        if ( 0 === r.length || common.isNullOrEmpty( r ) ) {
            console.warn( `Service client receiving service RPC response data but can not mapping requestId. Possible request '${requestId}' is timeout.` );
            return;
        }

        let request = r.firstOrDefault();

        if ( common.isNullOrEmpty( request ) ) {
            return;
        }

        try {
            request.response = responsePackage;
        }
        catch ( ex ) {
            console.error( `Set the RPC response package to RPC request occur exception. \n${ex.message}` );
            return;
        }
    } );
}

/** 發送 Socket 連線訊息
 * @description 發送 Socket 連線訊息
 * @param {Socket} socket TCP Socket 連線物件
 * @param {ResponsePackage} message Socket 訊息物件
 */
function sendSocketMessage( socket, message ) {
    let json        = jsonUtil.serialize( message );
    let jsonMessage = JSON.stringify( message ) + '\n';
    let rpcMessage  = new Buffer( jsonMessage );

    console.log( `Sending outgoing socket message: \n${json}`, { color : colors.magenta } );

    try {
        socket.write( rpcMessage );
    }
    catch ( ex ) {
        console.error( `Sending socket message occur exception. Socket connectionId is ${socket.connectionId}. \n${ex.message}` );
    }
}

module.exports = SocketEventHandler;