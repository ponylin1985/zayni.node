'use strict';

const common = require( '../common.js' ).common;

/** 服務動作基底。
 * 1. 子類別動作需要實作 execute() 抽象方法。
 * 2. 子類別的 execute() 實作必須要回傳 Promise 物件。
 * @description 服務端的動作基底樣板。
 * @class ServiceAction
 */
class ServiceAction {

    /** 建構子函式
     * @description 建立 ServiceAction 物件
     * @param {String} actionName 動作名稱
     * @memberOf ServiceAction
     */
    constructor( actionName ) {
        if ( this.constructor === ServiceAction ) {
            throw new Error( 'ServiceAction is an abstract class. Can not be instantiated directly.' );
        }

        if ( this.execute === undefined ) {
            throw new Error( "Does not implement 'execute' abstract function." );
        }

        if ( common.isNullOrEmpty( actionName ) ) {
            throw new Error( "Argument 'actionName' can not be null or empty string." );
        }

        /** 動作名稱
         * @description 動作名稱
         * @type {String}
         */
        this.actionName = actionName;

        /** 請求資料集合
         * @description 請求資料集合
         * @type {any}
         */
        this.requestData = null;

        /** 回應資料集合
         * @description 回應資料集合
         * @type {any}
         */
        this.responseData = null;

        /** 動作執行結果
         * @description 動作執行結果
         * @type {Result} 
         */
        this.result = null;
    }
}

module.exports = ServiceAction;