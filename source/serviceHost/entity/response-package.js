'use strict';

/** 遠端服務回應包裹
 * @description 遠端服務回應包裹
 * @class ResponsePackage
 */
class ResponsePackage {

    /** 建構子函式
     * @description 建立 RequestPackage 物件
     * @param {String} requestId 請求識別代碼
     * @param {String} messageType 訊息類型
     * @param {String} actionName 請求動作名稱
     * @param {Boolean} isSuccess 動作執行是否成功
     * @param {String} code 動作執行結果代碼
     * @param {any} data 資料內容集合
     * @param {String} message 執行結果訊息
     * @memberOf ResponsePackage
     */
    constructor( requestId, messageType, actionName, isSuccess, code, data, message ) {

        /** 請求識別代碼
         * @description 請求識別代碼
         * @type {String}
         */
        this.requestId = requestId;

        /** 訊息類型，支援的訊息種類如下: 
         * 1. RpcRequest: RPC 主動請求。
         * 2. RpcResponse: RPC 訊息回覆。
         * 3. Publish: 單向主動訊息推播。
         * @description 訊息類型
         * @type {String}
         */
        this.messageType = 'RpcRequest';

        /** 請求動作名稱
         * @description 請求動作名稱
         * @type {String}
         */
        this.actionName = actionName;

        /** 動作執行是否成功
         * @description 動作執行是否成功
         * @type {Boolean}
         */
        this.isSuccess = false;

        /** 動作執行結果代碼
         * @description 動作執行結果代碼
         * @type {String}
         */
        this.code = null;

        /** 資料內容集合
         * @description 資料內容集合
         * @type {any}
         */
        this.data = data;

        /** 執行結果訊息
         * @description 執行結果訊息
         * @type {String}
         */
        this.message = null;
    }
}

module.exports = ResponsePackage;