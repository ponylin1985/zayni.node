'use strict';

/** 遠端服務請求包裹
 * @description 遠端服務請求包裹
 * @class RequestPackage
 */
class RequestPackage {

    /** 建構子函式
     * @description 建立 RequestPackage 物件
     * @param {String} requestId 請求識別代碼
     * @param {String} messageType 訊息類型
     * @param {String} actionName 請求動作名稱
     * @param {any} data 資料內容集合
     * @param {Date} requestTime 請求時間戳記
     * @memberOf RequestPackage
     */
    constructor( requestId, messageType, actionName, data, requestTime ) {

        /** 請求識別代碼
         * @description 請求識別代碼
         * @type {String}
         */
        this.requestId = requestId;

        /** 訊息類型，支援的訊息種類如下: 
         * 1. RpcRequest: RPC 主動請求。
         * 2. RpcResponse: RPC 訊息回覆。
         * 3. Publish: 單向主動訊息推播。
         * @description 訊息類型
         * @type {String}
         */
        this.messageType = 'RpcResponse';

        /** 請求動作名稱
         * @description 請求動作名稱
         * @type {String}
         */
        this.actionName = actionName;

        /** 資料內容集合
         * @description 資料內容集合
         * @type {any}
         */
        this.data = data;

        /** 請求時間戳記
         * @description 請求時間戳記
         * @type {Date}
         */
        this.requestTime = requestTime;

        /** RPC 回應逾時的時間
         * @description RPC 回應逾時的時間
         * @type {Date}
         */
        this.timeoutTime = null;
    }
}

module.exports = RequestPackage;