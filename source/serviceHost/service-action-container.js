'use strict';

const common = require( '../common.js' ).common;
const Result = require( '../entity' ).Result;

/** 服務動作池
 * @description 存放服務端動作的容器。
 */
const _actions = new Map();

/** 服務動作容器
 * @description 控管服務端動作的容器
 * @type {ServiceActionContainer}
 */
const serviceActionContainer = {

    /** 新增服務動作
     * @description 註冊服務端的動作到容器中。
     * @param {ServiceAction} serviceAction 服務動作物件。
     * @returns {Result} 新增結果
     */
    addServiceAction : function ( serviceAction ) {
        let result = new Result( false );

        if ( common.isNullOrEmpty( serviceAction ) ) {
            result.message = "Argument invalid. Argument 'serviceAction' can not be null.";
            return result;
        }

        if ( !serviceAction.hasOwnProperty( 'actionName' ) ) {
            result.message = "Argument invalid. Argument 'serviceAction' must have 'actionName' property.";
            return result;
        }

        let actionName = null;

        try {
            actionName = serviceAction.actionName;
        }
        catch ( ex ) {
            result.message = `Argument invalid. Try get actionName from arg occur exception. \n${ex.message}`;
            return result;
        }

        if ( common.isNullOrEmpty( actionName ) ) {
            result.message = "Argument invalid. 'actionName' can not be null or empty in serviceaAction arg.";
            return result;
        }

        if ( _actions.has( actionName ) ) {
            result.isSuccess = true;
            return result;
        }

        try {
            _actions.set( actionName, serviceAction );
        }
        catch ( ex ) {
            result.message = `Put serviceAction instance into container occur exception. \n${ex.message}`;
            return result;
        }

        result.isSuccess = true;
        return result;
    },

    /** 取得服務動作
     * @description 從容器中取得指定名稱的服務動作。
     * @param {String} actionName 動作名稱。
     * @returns {ServiceAction} 服務動作物件。
     */
    getServiceAction : function ( actionName ) {
        if ( common.isNullOrEmpty( actionName ) ) {
            console.error( `Argument 'actionName' is null or empty.` );
            return null;
        }

        if ( !_actions.has( actionName ) ) {
            console.error( `Service action container didn't contain the '${actionName}' action.` );
            return null;
        }

        let actionType = null;

        try {
            actionType = _actions.get( actionName ).constructor;
        }
        catch ( ex ) {
            console.error( `Get '${actionName}' from service action container occur exception. \n${ex.message}` );
            return null;
        }

        let action = null;

        try {
            action = Object.create( actionType.prototype );
        }
        catch ( ex ) {
            console.error( `Create service action instance occur exception. Action name is ${actionName}. \n${ex.message}` );
            return null;
        }

        if ( common.isNullOrEmpty( action ) ) {
            console.error( `Service action instance is null. Action name is ${actionName}.` );
            return null;
        }

        return action;
    }
};

module.exports = serviceActionContainer;