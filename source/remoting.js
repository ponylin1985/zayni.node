'use strict';

const ServiceHost            = require( './serviceHost/service.js' );
const ServiceAction          = require( './serviceHost/service-action.js' );
const ServiceActionContainer = require( './serviceHost/service-action-container.js' );
const ServiceClient          = require( './serviceHost/client.js' );
const RequestPackage         = require( './serviceHost/entity/request-package.js' );
const ResponsePackage        = require( './serviceHost/entity/response-package.js' );

module.exports.ServiceHost            = ServiceHost;
module.exports.ServiceAction          = ServiceAction;
module.exports.ServiceActionContainer = ServiceActionContainer;
module.exports.ServiceClient          = ServiceClient;
module.exports.RequestPackage         = RequestPackage;
module.exports.ResponsePackage        = ResponsePackage;