'use strict';

const fs         = require( 'fs' );
const path       = require( 'path' );
const util       = require( 'util' );
const colors     = require( 'colors/safe' );
const extension  = require( './extensions.js' );
const zayni      = require( './common.js' );
const common     = zayni.common;
const fileHelper = zayni.fileHelp;

/** 日誌紀錄物件
 * @description 進行 Console、文字檔的日誌紀錄。
 */
const logger = {

    /** 預設日誌設定
     * @description 預設的日誌功能設定值。
     */
    config : {
        "enableConsoleLog" : true,
        "enableFileLog"    : false,
        "fileLogPath"      : '/log/zayni.trace.log',
        "logFileMaxSize"   : 5
    },

    /** 根據指定的設定 Config 進行日誌物件的初始化。
     * @description 根據指定的設定 Config 進行日誌物件的初始化。
     * @param {any} config 日誌設定值。
     */
    useConfig : function ( config ) {
        let fileLogPath = config.fileLogPath;
        let fileLogDir  = path.dirname( fileLogPath );
        common.isFalse( fs.existsSync( fileLogDir ), () => fs.mkdirSync( fileLogDir ) );

        let fileLog     = fs.createWriteStream( fileLogPath, { flags: 'a', defaultEncoding: 'utf8' } );
        let consoleLog  = process.stdout;

        /** 調整文字日誌檔案的檔案名稱
         * description 採用目前時間的Timestamp時間戳記當作Log File的檔案名稱。
         * @returns {String} 調整後的文字日誌檔案的完整路徑檔案名稱。
         */
        function adjustFileName() {
            let timestamp = common.currentLogTime().replaceAll( '-', '' ).replaceAll( ':', '' ).replaceAll( ' ', '_' );
            let index     = timestamp.indexOf( '.' );
            timestamp     = timestamp.substring( 0, index );
            return `${path.dirname( fileLogPath )}/debug_${timestamp}.log`;
        }

        /** 紀錄日誌訊息
         * @description 輸出程式日誌訊息到Console主控台或日誌檔案中。
         * @param {String} message 日誌訊息內容
         * @param {Colors} color Console文字顏色
         * @param {Boolean} withoutLogTitle 是否在Log File不顯示日誌時間與日誌層級訊息，預設為false。
         * @param {Boolean} consoleLogOnly 是否只於Console主控台輸出，預設為false。
         */
        function log( message, color, withoutLogTitle, consoleLogOnly ) {
            message = message || '';
            message = withoutLogTitle ? `${util.format( message )}\n` : `[${common.currentLogTime()}] ${util.format( message )}\n`;

            common.isTrue( config.enableFileLog && !consoleLogOnly, () => {
                let logFileMaxLimit = 1024 * 1024 * config.logFileMaxSize;  // 檔案大小單位為MB
                
                if ( fileHelper.getFileSize( fileLogPath ) > logFileMaxLimit ) {
                    fileLogPath = adjustFileName();
                    fileLog     = fs.createWriteStream( fileLogPath, { flags: 'w' } );
                }

                fileLog.write( message );
            } );

            common.isTrue( config.enableConsoleLog, () => {
                common.isNotNullOrEmptyThen( color, c => message = color( message ) );
                consoleLog.write( message );
            } );
        }

        /** 紀錄日誌訊息
         * @description 一般日誌訊息紀錄
         * @param {String} message 日誌訊息內容
         * @param {Object} options 日誌訊息紀錄參數集合
         * {
         *   color: null,             // 於Console輸出的字體顏色。
         *   withoutLogTitle: false,  // 是否在Log File不顯示日誌時間與日誌層級訊息，預設為false。
         *   consoleLogOnly: false    // 是否只於Console主控台輸出，預設為false。
         * }
         */
        console.log = function ( message, options ) {
            options = options || {
                color           : null,
                withoutLogTitle : false,
                consoleLogOnly  : false
            };

            let color           = options.color           || null;
            let withoutLogTitle = options.withoutLogTitle || false;
            let consoleLogOnly  = options.consoleLogOnly  || false;

            message = withoutLogTitle ? message : `[Log] ${message}`;
            log( message, color, withoutLogTitle, consoleLogOnly );
        };

        /** 紀錄錯誤或異常訊息
         * @description 錯誤或異常訊息紀錄
         * @param {String} message 日誌訊息內容
         * @param {Colors} color Console文字顏色
         */
        console.error = function ( message, color ) {
            color = color || colors.red;
            log( `[Error] ${message}`, color );
        };

        /** 紀錄一般訊息
         * @description 一般程式訊息紀錄
         * @param {String} message 日誌訊息內容
         * @param {Colors} color Console文字顏色
         */
        console.info = function ( message, color ) {
            color = color || colors.green;
            log( `[Info] ${message}`, color );
        };

        /** 紀錄警告訊息
         * @description 警告訊息紀錄
         * @param {String} message 日誌訊息內容
         * @param {Colors} color Console文字顏色
         */
        console.warn = function ( message, color ) {
            log( `[Warn] ${message}` );
        };

        /** 紀錄除錯模式訊息
         * @description 偵錯模式訊息紀錄
         * @param {String} message 日誌訊息內容
         * @param {Colors} color Console文字顏色
         */
        console.debug = function ( message, color ) {
            log( `[Debug] ${message}` );
        };
    }
};

module.exports = logger;