'use strict';

const fs     = require( 'fs' );
const common = require( './common.js' );

/** 檔案工具物件
 * @description 檔案工具物件
 */
const fileHelper = {

    /** 取得檔案的大小
	 * @description 取得指定檔案的大小，單位為位元組。假若回傳值為Null，代表發生異常。
	 * @param {String} fileFullPath 檔案的完整路徑
	 * @returns {Number} 檔案的大小，單位為位元組
	 */
    getFileSize : function ( fileFullPath ) {
        if ( common.isNullOrEmpty( fileFullPath ) ) {
            return null;
        }

        let fileSize = 0;

        try {
            fileSize = fs.statSync( fileFullPath )[ "size" ];
        } 
        catch ( ex ) {
            return null;		
        }
        
        return fileSize;
    }
};

module.exports = fileHelper;