'use strict';

const fs = require( 'fs' );

/** 檢查變數是否指向為undefined，或指向Null、空字串、空陣列。
 * @param {Object} target 目標變數
 * @returns {Boolean} 是否指向為undefined，或指向Null、空字串、空陣列。
 */
function isNullOrEmpty( target ) {
	return null === target || 'undefined' === typeof ( target ) || '' === target || 0 === target.length;
}

/** 檢查變數是否「不」為undefined，或指向Null、空字串。
 * @param {Object} target 目標變數
 * @returns {Boolean} 是否不指向為undefined，或指向Null、空字串。
 */
function isNotNullOrEmpty( target ) {
	return 'undefined' !== typeof ( target ) && null !== target && '' !== target;
}

/** 常用函式庫物件
 * @description 函式庫集合物件
 */
const common = {

    /** 產生目前時間的Log字串
	 * @description 時間格式為 yyyy-MM-dd HH:mm:ss.fff
     * @returns {String} 目前時間的Log字串
     */
    currentLogTime : function () {
        let date      = new Date();
        let delimiter = "-";

        let month = date.getMonth() + 1 + '';

		if ( month.length < 2 ) {
			month = '0' + month;
		}

		let day = date.getDate() + '';

		if ( day.length < 2 ) {
			day = '0' + day;
		}

		let hour = date.getHours() + '';

		if ( hour.length < 2 ) {
			hour = '0' + hour;
		}

		let minute = date.getMinutes() + '';

		if ( minute.length < 2 ) {
			minute = '0' + minute;
		}

		let second = date.getSeconds() + '';

		if ( second.length < 2 ) {
			second = '0' + second;
		}

		let millisecond = date.getMilliseconds() + '';

		switch ( millisecond.length ) {
			case 1:
				millisecond = '00' + millisecond;
				break;

			case 2:
				millisecond = '0' + millisecond;
				break;
		}

        return `${date.getFullYear()}-${month}-${day} ${hour}:${minute}:${second}.${millisecond}`;
    },

    /** 檢查變數是否指向為undefined，或指向Null、空字串、空陣列。
     * @param {Object} target 目標變數
     * @param {Object} defaultValue 預設值
     * @returns {Boolean} 是否指向為undefined，或指向Null、空字串、空陣列。
     */
    isNullOrEmpty : function ( target, defaultValue ) {
		// 20160822 Pony Says: 這邊原本用ES6 Arrow Function的寫法，不支援 arguments 屬性... 要小心...
		if ( 0 === arguments.length ) {
			return;
		}

		if ( 1 === arguments.length ) {
			return isNullOrEmpty( target );
		}

		if ( 2 === arguments.length ) {
			if ( isNullOrEmpty( target ) ) {
				return defaultValue;
			}

			return target;
		}
    },

	/** 檢查目標變數是否指向undefined、Null、空字串、空陣列，如果是的會自動執行傳入的動作回呼函式。
     * @param {Object} target 目標變數
     * @param {function} action 動作回呼函式
     */
	isNullOrEmptyThen : function ( target, action ) {
		if ( isNullOrEmpty( target ) && action ) {
			action();
		}
	},

	/** 檢查變數是否「不」undefined，或指向Null、空字串、空陣列。
	 * @param {Object} target 目標變數
	 * @returns {Boolean} 是不否指向為undefined，或指向Null、空字串、空陣列。
	 */
    isNotNullOrEmpty : function ( target ) {
        return 'undefined' !== typeof ( target ) && null !== target && '' !== target;
    },

	/** 如果變數是否「不」為undefined，或指向Null、空字串、空陣列，則自動執行傳入的回呼函式。
	 * @param {Object} target 目標變數
	 * @param {function} action 動作回呼函式，參數為原本的目標變數。
	 */
	isNotNullOrEmptyThen : function ( target, action ) {
		if ( isNotNullOrEmpty( target ) && action ) {
			action( target );
		}
	},

	/** 檢查條件是否成立為 true 值，假若成立則自動執行傳入的動作回呼
     * @description 檢查條件是否成立為 true 值，假若成立則自動執行傳入的動作回呼。
     * @param {boolean} condition 布林條件運算結果。
     * @param {function} action 待執行的動作回呼函式。
     */
    isTrue : function ( condition, action ) {
        if ( condition && action ) {
            action();
        }
    },

    /** 檢查條件是否成立為 false 值，假若成立則自動執行傳入的動作回呼
     * @description 檢查條件是否成立為 false 值，假若成立則自動執行傳入的動作回呼。
     * @param {boolean} condition 布林條件運算結果。
     * @param {function} action 待執行的動作回呼函式。
     */
    isFalse : function ( condition, action ) {
        if ( !condition && action ) {
            action();
        }
    },

	/** 取得目前時間的 Unix UTC Timestamp 時間戳記。
	 * @description 取得目前時間的 Unix UTC Timestamp 時間戳記。
	 * @returns {Number} Unix UTC Timestamp 時間戳記。
	 */
	getCurrentTimestamp : function () {
		return this.getTimestamp( new Date() );
	},

	/** 取得目前時間的 Unix UTC Timestamp 時間戳記，包括毫秒數。
	 * @description 取得目前時間的 Unix UTC Timestamp 時間戳記，包括毫秒數。
	 * @returns {Number} Unix UTC Timestamp 時間戳記，包括毫秒數。
	 */
	getCurrentTimestampMillisecond : function () {
		return this.getTimestampMillisecond( new Date() );
	},

	/** 取得指定時間的 Unix UTC Timestamp 時間戳記。
	 * @description 取得指定時間的 Unix UTC Timestamp 時間戳記。
	 * @param {Date} dateTime 目標時間
	 * @returns {Number} Unix UTC Timestamp 時間戳記。
	 */
	getTimestamp : function ( dateTime ) {
		return Math.floor( dateTime.getTime() / 1000 );
	},

	/** 取得指定時間的 Unix UTC Timestamp 時間戳記，包括毫秒數。
	 * @description 取得指定時間的 Unix UTC Timestamp 時間戳記，包括毫秒數。
	 * @param {Date} dateTime 目標時間
	 * @returns {Number} Unix UTC Timestamp 時間戳記，包括毫秒數。
	 */
	getTimestampMillisecond : function ( dateTime ) {
		return dateTime.getTime();
	},

	/** 轉換 UTC 時間為 Local 本地時間
     * @description 轉換 UTC 時間為 Local 本地時間。
     * @param {Date} utcTime 原始 UTC 時間
     * @returns {Date} Local 時區的本地時間。
     */
    convertToLocalTime : function convertToLocalTime( utcTime ) {
        let timezoneOffset = utcTime.getTimezoneOffset();
        utcTime.setMinutes( utcTime.getMinutes() - timezoneOffset );
        return utcTime;
    },

	/** 產生亂數的字串
	 * @description 產生亂數的字串，亂數字元包括數字、大小寫英文字母。
	 * @param {Number} length 亂數字串的長度，必須大於 0
	 * @returns {String} 亂數字串。
	 */
	createRandomText : function ( length ) {
		if ( length <= 0 ) {
			return '';
		}

		let text  = '';
		let chars = 'BwxYDXflqEFCHagdGeibcjIJKA7h8LMno2pNOR345mQVWrs6STUPtuvyz01Zk9';

		for ( let i = 0; i < length; i++ ) {
			text += chars.charAt( Math.floor( Math.random() * chars.length ) );
		}

		return text;
	}
};

module.exports = common;