'use strict';

const uuid = require( 'uuid/v4' );

/** GUID 識別公用物件
 * @description GUID 識別公用物件
 */
const guid = {

    /** 產生 GUID 字串
     * @description 產生 GUID 字串
     * @returns {String} 產生出的 GUID 字串
     */
    createGuid : function () {
        return uuid();
    },

    /** 產生 GUID 字串
     * @description 產生 GUID 字串
     * @returns {String} 產生出的 GUID 字串
     */
    createGuid_obsolete : function () {
        function radomText() { 
            return ( ( ( 1 + Math.random() ) * 0x10000 ) | 0 ).toString( 16 ).substring( 1 );
        }
    
        return ( radomText() + radomText() + "-" + radomText() + "-4" + radomText().substr( 0, 3 ) + "-" + radomText() + "-" + radomText() + radomText() + radomText() ).toLowerCase();
    }
};

module.exports = guid;