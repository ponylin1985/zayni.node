'use strict';

/** 執行結果類別
 * @description 執行結果類別
 * @class Result
 */
class Result {

    /** 建構子函式
     * @description 建立 Result 物件實體。
     * @param {Boolean} isSuccess 執行是否成功
     * @memberOf Result
     */
    constructor( isSuccess ) {
        
        /** 執行是否成功
         * @description 執行結果是否成功。
         * @type {Boolean}
         */
        this.isSuccess = isSuccess || false;

        /** 執行結果的代碼
         * @description 執行結果的代碼。
         * @type {String}
         */
        this.code = null;

        /** 執行結果的訊息
         * @description 執行結果的訊息。
         * @type {String}
         */
        this.message = null;

        /** 資料集合
         * @description 資料集合
         * @type {any}
         */
        this.data = null;
    }
}

module.exports = Result;