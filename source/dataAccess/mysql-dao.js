'use strict';

const mysql      = require( 'mysql' );
const extensions = require( '../extensions.js' );
const common     = require( '../common.js' ).common;

/** MySQL資料存取物件基底
 * @description 資料存取層的物件。
 * @returns {MySQLDao} 資料存取層的物件
 */
const mySqlDao = ( function () {

    /** MySQL 資料庫連線池
     * @description 內部的 MySQL 資料庫連線池
     */
    let _connectionPool = null;

	/** 建立資料庫連線
	 * @description 建立MySQL資料庫的連線，並且將資料庫連線開啟。
     * @param {any} connection MySQL 資料庫連線資訊。
	 * @returns {Promise} 建立資料庫連線Promise結果。resolve回呼函式參數為MySQL資料庫連線物件。
	 */
	function createConnection( connection ) {
		return new Promise( ( resolve, reject ) => {
            if ( common.isNotNullOrEmpty( connection ) ) {
                return reject( new Error( `Argument 'connection' can not be null or undefinded.` ) );
            }

			let db = null;

			try {
				db = mysql.createConnection( {
					host	 : connection.server,
					user	 : connection.uid,
					password : connection.password,
					port	 : connection.port,
					database : connection.database
				} );
			} 
			catch ( ex ) {
				console.error( `Create MySQL database connection occur exception: ${ex.message}` );
				return reject( ex );
			}

			db.connect( error => {
				if ( error ) {
					console.error( `Create MySQL database connection occur exception: ${error}` );
					return reject( error );
				}
			} );

			return resolve( db );
		} );
    }
    
    /** 取得 MySQL 資料庫連線
     * @description 從 MySQL 資料庫連線池中取得資料庫連線。
     * @param {any} cfg MySQL 資料庫連線資訊。
     * @returns {Promise} 建立資料庫連線 Promise 結果。resolve 回呼函式參數為MySQL資料庫連線物件。
     */
    function getConnection( cfg ) {
        return new Promise( ( resolve, reject ) => {
            if ( common.isNullOrEmpty( cfg ) ) {
                return reject( new Error( `Argument 'cfg' can not be null or undefinded.` ) );
            }

            if ( common.isNullOrEmpty( _connectionPool ) ) {
                try {
                    _connectionPool = mysql.createPool( {
                        host	 : cfg.server,
                        user	 : cfg.uid,
                        password : cfg.password,
                        port	 : cfg.port,
                        database : cfg.database
                    } );
                } catch ( ex ) {
                    console.error( `Create MySQL database connection pool occur exception. ${ex.message}` );
				    return reject( ex );
                }
            }

            _connectionPool.getConnection( ( error, db ) => {
                if ( error ) {
                    console.error( `Create MySQL database connection occur exception. ${error}` );
                    return reject( error );
                }

                return resolve( db );
            } );
		} );
    }

	/** 關閉資料庫連線
	 * @description 將傳入的MySQL資料庫連線關閉。
	 * @param db {DbConnection} MySQL資料庫連線物件。
	 * @returns {Promise} 關閉資料庫連線的Promise結果。  
	 */
	function closeConnection( db ) {
		return new Promise( ( resolve, reject ) => {
			if ( common.isNullOrEmpty( db ) ) {
				return reject( new Error( `Argument 'db' can not be null or undefinded.` ) );
			}

			try {
				db.end();
			}
			catch ( ex ) {
				console.error( `Close database connection occur exception: ${ex.message}` );
				return reject( ex );
			}

			return resolve( db );
		} );	
	}

	/** 開啟資料庫交易
	 * @description 開啟MySQL資料庫交易。
	 * @param db {DbConnection} MySQL資料庫連線物件。
	 * @returns {Promise} 開啟資料庫交易的Promise結果，resolve回呼函式參數為MySQL資料庫連線物件。
	 */
	function beginTransaction( db ) {
		return new Promise( ( resolve, reject ) => {
			if ( common.isNullOrEmpty( db ) ) {
				return reject( new Error( `Argument 'db' can not be null or undefinded.` ) );
			}

			db.beginTransaction( function ( error ) {
				if ( error ) {
					console.error( `Begin database transaction occur exception: ${error}` );
					return reject( error );
				}

				return resolve( db );
			} );
		} );
	}

	/** 確認資料庫交易
	 * @description 確認MySQL資料庫的交易。
	 * @param db {DbConnection} MySQL資料庫連線物件。
	 * @returns {Promise} 確認資料庫交易的Promise結果，resolve回呼函式參數為MySQL資料庫連線物件。
	 */
	function commit( db ) {
		return new Promise( ( resolve, reject ) => {
			if ( common.isNullOrEmpty( db ) ) {
				return reject( new Error( `Argument 'db' can not be null or undefinded.` ) );
			}

			db.commit( function ( ex ) {
				if ( ex ) {
					console.error( `Commit database transaction occur exception: ${ex}` );
					return reject( ex );
				}

				return resolve( db );
			} );
		} );
	}

	/** 退回資料庫交易
	 * @description 退回MySQL資料庫交易。
	 * @param db {DbConnection} MySQL資料庫連線物件。
	 * @returns {Promise} 退回資料庫交易的Promise結果，resolve回呼函式參數為MySQL資料庫連線物件。
	 */
	function rollback( db ) {
		return new Promise( ( resolve, reject ) => {
			if ( null === db || 'undefined' === db ) {
				return reject( new Error( `Argument 'db' can not be null or undefinded.` ) );
			}

			db.rollback( function ( ex ) {
				if ( ex ) {
					console.error( `Rollback database transaction occur exception: ${ex}` );
					return reject( ex );
				}

				return resolve( db );
			} );
		} );
	}

	/** 執行資料庫SQL指令
	 * @description 執行SQL指令。
	 * @param {DbConnection} db MySQL資料庫連線物件
	 * @param {String} sql SQL指令字串
	 * @param {Object} parameters SQL指令參數集合
	 * @returns {Promise} 執行SQL指令的Promise結果，resolve回呼函式的參數，會包含執行完成後的MySQL結果集合。
	 */
	function execute( db, sql, parameters ) {
		return new Promise( ( resolve, reject ) => {
			db.query( sql, parameters, ( error, result ) => {
				if ( error ) {
					console.error( `Excute sql command occur exception:\n${sql}\n${error}` );
					return reject( error );
				}

				db.result = result;
				return resolve( db );
			} );
		} );
	}

	/** MySQL資料庫的資料存取物件
	 * @description MySQL資料庫的資料存取基礎物件
	 */
	const mysqlDao = {

		/** 建立資料庫連線
		 * @description 建立MySQL資料庫的連線，並且將資料庫連線開啟。
         * @param {any} connection MySQL 資料庫連線資訊。
		 * @returns {Promise} 建立資料庫連線Promise結果。resolve回呼函式參數為MySQL資料庫連線物件。
		 */
		connect	: createConnection,

		/** 關閉資料庫連線
		 * @description 將傳入的MySQL資料庫連線關閉。
		 * @param db {DbConnection} MySQL資料庫連線物件。
		 * @returns {Promise} 關閉資料庫連線的Promise結果。  
		 */
        disconnect : closeConnection,
        
        /** 取得資料庫連線
         * @description 從 MySQL 資料庫連線池中取得資料庫連線。
         * @param {any} cfg MySQL 資料庫連線資訊。
         * @returns {Promise} 建立資料庫連線 Promise 結果。resolve 回呼函式參數為MySQL資料庫連線物件。
         */
        getConnection : getConnection,

		/** 開啟資料庫交易
		 * @description 開啟MySQL資料庫交易。
		 * @param db {DbConnection} MySQL資料庫連線物件。
		 * @returns {Promise} 開啟資料庫交易的Promise結果，resolve回呼函式參數為MySQL資料庫連線物件。
		 */
		beginTransaction : beginTransaction,

		/** 確認資料庫交易
		 * @description 確認MySQL資料庫的交易。
		 * @param db {DbConnection} MySQL資料庫連線物件。
		 * @returns {Promise} 確認資料庫交易的Promise結果，resolve回呼函式參數為MySQL資料庫連線物件。
		 */
		commit : commit,

		/** 退回資料庫交易
		 * @description 退回MySQL資料庫交易。
		 * @param db {DbConnection} MySQL資料庫連線物件。
		 * @returns {Promise} 退回資料庫交易的Promise結果，resolve回呼函式參數為MySQL資料庫連線物件。
		 */
		rollback : rollback,

		/** 執行資料庫SQL指令
		 * @description 執行SQL指令。
		 * @param {DbConnection} db MySQL資料庫連線物件
		 * @param {String} sql SQL指令字串
		 * @param {Object} parameters SQL指令參數集合
		 * @returns {Promise} 執行SQL指令的Promise結果，resolve回呼函式的參數，會包含執行完成後的MySQL結果集合。
		 */
		execute : execute
	};

	return mysqlDao;
}() );

module.exports = mySqlDao;