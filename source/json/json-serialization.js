'use strict';

const zayni  = require( '../common.js' );
const log    = require( '../log.js' );
const common = zayni.common;

/** Camel-Case 的 JSON 屬性名稱處理。
 * @description Camel-Case 的 JSON 屬性名稱處理。
 * @param {any} key JSON 屬性名稱
 * @param {any} value JSON 屬性值。
 */
function camelCaseReviver( key, value ) {
    if ( value && typeof value === 'object' ) {
        for ( let k in value ) {
            if ( /^[A-Z]/.test( k ) && Object.hasOwnProperty.call( value, k ) ) {
                value[ k.charAt( 0 ).toLowerCase() + k.substring( 1 )] = value[ k ];
                delete value[ k ];
            }
        }
    }

    return value;
}

/** JSON 序列化處理類別
 * @description 序列化處理類別
 * @class JsonConverter
 */
class JsonConverter {

    /** 序列化 JavaScript 物件成 JSON 格式字串。
     * @description 
     * 1. 序列化 JavaScript 物件成 JSON 格式字串。
     * 2. 可依照參數指定 JSON 的縮排空格數，假若未傳入，則預設以 2 個空格當作縮排。
     * 3. 以 Camel-Case 風格屬性命名輸出。
     * @static
     * @param {any} obj JavaScript 物件。
     * @param {Number} indentation 縮排空格數，假若未傳入，則預設以 2 個空格當作縮排。
     * @returns {String} JSON 格式的字串。
     * @memberOf JsonConverter
     */
    static serialize( obj, indentation ) {
        if ( common.isNullOrEmpty( obj ) ) {
            console.log( `Serialize from object to JSON error. Argument 'obj' can not be null.` );
            return null;
        }

        if ( common.isNullOrEmpty( indentation ) ) {
            indentation = 2;
        }

        try {
            return JSON.stringify( obj, camelCaseReviver, indentation );
        }
        catch ( ex ) {
            console.log( `Serialize from object to JSON error. ${ex.message}` );
            throw ex;
        }
    }
    
    /** 反序列化 JSON 字串成 JavaScript 物件或陣列。
     * @description 反序列化 JSON 字串成 JavaScript 物件或陣列。
     * 1. 以 Camel-case 風格屬性命名。
     * @static 
     * @param {any} json JSON 格式的字串。
     * @returns {any} JavaScript 的物件或是陣列。
     * @memberOf JsonConverter
     */
    static deserialize( json ) {
        if ( common.isNullOrEmpty( json ) ) {
            console.log( `Deserialize from JSON to object error. Argument 'json' can not be null or empty string.` );
            return null;
        }

        try {
            return JSON.parse( json, camelCaseReviver );
        }
        catch ( ex ) {
            console.log( `Deserialize from JSON to object error. ${ex.message}` );
            throw ex;
        }
    }
}

module.exports = JsonConverter;