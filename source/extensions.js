﻿/* ================================================================================================

Author: Pony Lin
Created Date: 2013/07/15
Version: 1.2.15.03
Description: 此為 zayni.node 框架內部對一些 JavaScript 原生的物件實作的一些 extension 擴充方法的 API Library。

===================================================================================================

這邊指暫時引用 fusion.extension.js 針對 String、Array 物件的擴充，以後有需要再慢慢引用其他的擴充，例如: Object、Date、Function... 等等。
還有像 String Base64、gzip library的擴充API。
但由於 fusion.js 和 fusion.extension.js 原本的執行環境上下文是設計針對瀏覽器端，後端服務 Node.js 原生內建的API模組，可能已經有支援像是Base64、zip等函式庫，
所以暫時不全不引用完整 fusion.extension.js 檔案。

====================================================================================================== */

'use strict';

module.exports.objectExtension = require( './extensions/object-extensions.js' );
module.exports.stringExtension = require( './extensions/string-extensions.js' );
module.exports.arrayExtension  = require( './extensions/array-extensions.js' );