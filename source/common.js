﻿'use strict';

const common   = require( './common/common.js' );
const guid     = require( './common/guid.js' );
const fileHelp = require( './common/file-help.js' );

module.exports.common   = common;
module.exports.guid     = guid;
module.exports.fileHelp = fileHelp;