'use strict';

( function () {
    
    if ( "undefined" === typeof ( Array.isArray ) ) {

        //<summary>檢查傳入的目標物件，是否確實為一個JavaScript陣列
        //</summary>
        //<param name="target">目標物件</param>
        Array.isArray = function ( target ) {
            /// <summary>檢查傳入的目標物件，是否確實為一個JavaScript陣列
            /// </summary>
            /// <param name="target">目標物件</param>
            /// <returns type="boolean">是否為陣列</returns>

            let result = "[object Array]" === Object.prototype.toString.call( target );
            return result;
        };
    }

    /** 檢查JavaScript的陣列是否為一個長度為 0 的空陣列 (傳入的物件必須已經為一個陣列)
     * @description 是否為一個長度為 0 的空陣列。
     * @returns {Boolean} 是否為一個長度為 0 的空陣列。
     */
    Array.prototype.isNullOrEmptyArray = function () {
        let thiz = this;

        if ( 0 === thiz.length ) {
            return true;
        }

        return false;
    };

    /** 檢查JavaScript陣列中是否有包含任何空字串、Undefined或Null值的元素。
     * @description 陣列中是否有包含任何空字串、Undefined或Null值的元素。
     * @returns {Boolean} 是否有包含任何空字串、Undefined或Null值的元素。
     */
    Array.prototype.hasNullOrEmptyElements = function () {
        for ( let i = 0, len = this.length; i < len; i++ ) {
            let target = this[ i ];
            
            if ( null === target || 'undefined' === typeof ( target ) || '' === target ) {
                return true;
            }
        }
        
        return false;
    };

    /** 移除JavaScript陣列中指定的元素
     * @description 從陣列中移除指定的元素
     * @param {Object} target 待移除的目標元素
     */
    Array.prototype.remove = function ( target ) {
        for ( let i = 0, len = this.length; i < len; i++ ) {
            if ( target === this[ i ] ) {
                this.splice( i, 1 );
                return;
            }
        }
    };

    /** 取得JavaScript陣列中第一個元素 (類似.NET System.Linq的First方法)
     * @description 取得陣列中的第一個元素，假若無法正常取得第一個元素值，可能會拋出程式異常。
     * @returns {Object} 第一個元素值。
     */
    Array.prototype.first = function () {
        let result = this[ 0 ];
        return result;
    };

    /** 取得JavaScript陣列中第一個元素，如果無法正常取得第一個元素值，回傳null值 (類似.NET System.Linq的FirstOrDefault方法)
     * @description 取得陣列中的第一個元素，如果無法正常取得第一個元素值，回傳null值。
     * @returns {Object} 第一個元素值。
     */
    Array.prototype.firstOrDefault = function () {
        let sLength = this.length;
        
        if ( 0 === sLength ) {
            return null;
        }
        
        let result = this[ 0 ];
        
        if ( null === result || 'undefined' === typeof ( result ) || '' === result ) {
            return null;
        }
        
        return result;
    };

    /** 取得JavaScript陣列中最後一個元素 (類似.NET System.Linq的Last方法)
     * @description 取得JavaScript陣列中最後一個元素，假若無法正常取得最後一個元素值，可能會拋出程式異常。
     * @returns {Object} 最後一個元素值。
     */
    Array.prototype.last = function () {
        let result = this[ this.length - 1 ];
        return result;
    };

    /** 取得JavaScript陣列中最後一個元素，如果無法正常取得最後一個元素值，回傳null值 (類似.NET System.Linq的LastOrDefault方法)
     * @description 取得JavaScript陣列中最後一個元素。
     * @returns {Object} 最後一個元素值。
     */
    Array.prototype.lastOrDefault = function () {
        let result = this[ this.length - 1 ];
        
        if ( null === result || 'undefined' === typeof ( result ) || '' === result ) {
            return null;
        }
        
        return result;
    };

    /** 回傳去除掉JavaScript陣列中重複的元素的新陣列，但不能針對複雜型態(JavaScript物件)的陣列元素 (類似.NET System.Linq的Distinct方法)
     * @description 針對JavaScript基本資料型別的陣列，可以去除掉陣列中重複元素，並且回傳。
     * @example
     * let arr = [ 1, 1, 1, 2, 2, 3 ].distinct();
     * console.log( arr );      // [ 1, 2, 3 ]
     * @returns {Array} 去除掉重複元素的新陣列。
     */
    Array.prototype.distinct = function() {
        let temp   = {};
        let result = [];
        
        for ( let i = 0, len = this.length; i < len; i++ ) {
            let m = this[ i ];

            if ( temp.hasOwnProperty( m ) ) {
                continue;
            }
        
            result.push( this[ i ] );
            temp[ m ] = 1;
        }
        
        return result;
    };

    /** 回傳一個去除掉JavaScript陣列中重複的元素的新陣列，可以針對複雜型態的陣列元素進行多個欄位進行distinct (類似.NET System.Linq的Distinct方法)
     * @description 針對JavaScript物件的陣列，可以去除掉陣列中重複元素，並且回傳。
     * @example
     * let arr = [ {
     *     name : 'Kate',
     *     age : 23,
     *     sex : 'female'
     * }, {
     *     name : 'Amy',
     *     age : 21,
     *     sex : 'female'
     * }, {
     *     name : 'Pony',
     *     age : 30,
     *     sex : 'male'
     * } ];
     * 
     * arr = arr.distinctElement( 'sex' );      // 去除掉 sex 屬性重複的元素。
     * console.log( arr );                      // 只剩下 sex 屬性值為 male的元素。
     * @returns {Array} 去除掉重複元素的新陣列。
     */
    Array.prototype.distinctElement = function () {
        let temp   = {},
            result = [];

        let args = Array.prototype.slice.call( arguments );

        for ( let i = 0, len = this.length; i < len; i++ ) {
            let m = this[ i ]; 
            
            for ( let j = 0, aLen = args.length; j < aLen; j++ ) {
                let name  = args[ j ];
                let value = m[ name ];

                if ( !temp.hasOwnProperty( name ) && value !== temp[ name ] ) {
                    result.push( m );
                    temp[ name ] = value;
                }
            }
        }
        
        return result;
    };

    /** 檢查陣列中是否有重複的元素，如果有重複回傳true，沒有重複則回傳false
     * @description 檢查陣列中是否有重複的元素
     * @returns {Boolean} 檢查陣列中是否有重複的元素
     */
    Array.prototype.hasDuplicateElement = function () {
        let orgLength     = this.length;    
        let distinctArray = this.distinct();
        let newLength     = distinctArray.length;
        let result        = orgLength > newLength;
        return result;
    };

    /** JavaScript陣列中是否包含指定的目標元素 (類似.NET System.Linq的Contains方法)
     * @description 陣列中是否包含指定的目標元素
     * @param {Object} target 目標元素物件
     * @returns {Boolean} 是否包含指定的目標元素
     */
    Array.prototype.contains = function ( target ) {
        for ( let i = 0, iLength = this.length; i < iLength; i++ ) {
            if ( target === this [ i ] ) {
                return true;
            }
        }

        return false;
    };

    /** JavaScript陣列中是否有任何符合指定的布林運算式的元素 (類似.NET System.Linq的Any方法)
     * @description JavaScript陣列中是否有任何符合指定的布林運算式的元素
     * @param {Function} expression 布林條件運算式函式，此函式接受的參數為陣列中的元素
     * @returns {Boolean} 是否有包含指定條件的元素
     */
    Array.prototype.any = function ( expression ) {
        if ( null === expression || 'undefined' === typeof ( expression ) || '' === expression ) {
            throw new Error( "Arguments 'expression' can not be null, undefined or empty." );
        }

        if ( 'function' !== typeof ( expression ) ) {
            throw new Error( "Arguments 'expression' must be a function expression." );
        }

        for ( let i = 0, iLength = this.length; i < iLength; i++ ) {
            let m = this[ i ];
            
            if ( expression( m ) ) {
                return true;
            }
        }

        return false;
    };

    /** 根據指定的條件進行陣列元素的篩選。(類似.NET System.Linq的Where方法)
     * @description 篩選陣列的元素。
     * @param {Function} expression 回傳Boolean值的條件運算函式，此函式的參數為來源陣列中的元素。
     * @returns {Array} 篩選過後的陣列。
     */
    Array.prototype.where = function ( expression ) {
        let result = [];

        if ( null === expression || 'undefined' === typeof ( expression ) || '' === expression ) {
            throw new Error( "Arguments 'expression' can not be null, undefined or empty." );
        }

        if ( 'function' !== typeof ( expression ) ) {
            throw new Error( "Arguments 'expression' must be a function expression." );
        }

        for ( let i = 0, len = this.length; i < len; i++ ) {
            let m = this[ i ];
            
            if ( expression( m ) ) {
                result.push( m );
            }
        }

        return result;
    };

    /** 根據指定的條件選取陣列中的元素到新的陣列中。 (類似.NET System.Linq的Select方法)
     * @description 依照指定的運算式條件，選取來源陣列中的元素，或元素中的Property屬性，當作新陣列的成員，並且回傳新陣列。
     * @param {Function} expression 選取陣列元素的運算式函式。
     * @returns {Array} 選取欄位結果的新陣列。
     */
    Array.prototype.select = function ( expression ) {
        /// <summary>根據傳入的運算式函式，選取JavaScript陣列中元素的屬性到一個新個陣列中 (類似.NET System.Linq的Select方法)
        /// </summary>
        /// <param name="expression">運算式函式，此函式接受的參數為陣列中的元素</param>
        /// <returns type="array">選取欄位結果的新陣列</returns>
        
        let result = [];

        if ( null === expression || 'undefined' === typeof ( expression ) || '' === expression ) {
            throw new Error( "Arguments 'expression' can not be null, undefined or empty." );
        }

        if ( 'function' !== typeof ( expression ) ) {
            throw new Error( "Arguments 'expression' must be a function expression." );
        }

        for ( let i = 0, len = this.length; i < len; i++ ) {
            let element = this[ i ];
            let r = expression( element );
            
            if ( null !== r && 'undefined' !== typeof ( r ) && '' !== r ) {
                result.push( r );
            } 
        }
        
        return result;
    };

    /** 根據傳入的迭代函式，對JavaScript陣列進行迭代處理，傳入的回呼函式一定要return迭代變數 (類似.NET System.Linq的ForEach方法)
     * @param {Function} handler 對陣列的迭代函式，此函式接受的參數為陣列中的元素
     */
    Array.prototype.each = function ( handler ) {
        for ( let i = 0, thisLength = this.length; i < thisLength; i++ ) {
            let m = this[ i ];
            
            try {
                let result = handler( m );

                if ( 'undefined' !== typeof ( result ) ) {
                    this[ i ] = result;
                }
            }
            catch ( ex ) {
                throw ex;
            }
        }
    };

    /** 將指定的物件加入到陣列的尾端。
     * @description 將新的元素加入到陣列的尾端。
     * @param {Object} target 新的陣列元素物件。
     */
    Array.prototype.append = function ( target ) {
        if ( 0 === this.length ) {
            this[ 0 ] = target;
            return;
        }
        
        this[ this.length + 1 ] = target;
    };
}() );