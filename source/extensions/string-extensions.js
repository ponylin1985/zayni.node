'use strict';

( function () {
    
    /** 對來源字串進行格式化
     * @description 依照字串中指定的取代字符，進行字串的格式化。
     * @example
     * let result = String.format( 'My name is {user name}. I am {age}. {Hello World}', { 'user name': 'Pony Lin', 'age': 31, 'Hello World': 'How are you doing?' } );
     * console.log( result );   // My name is Pony Lin. I am 31. How are you doing?
     * let r = String.format( 'http://localhost:2579/api/ib/account/{account}/order/', { account : 'DU333231' } );
     * console.log( r );        // http://localhost:2579/api/ib/account/DU333231/order/
     * @param {String} source 來源字串
     * @param {Object} parameters 格式化參數集合
     * @returns {String} 格式化後的字串。
     */
    function stringFormat( source, parameters ) {
        for ( let name in parameters ) {
            if ( !parameters.hasOwnProperty( name ) ) {
                continue;
            }

            let value = parameters[ name ];
            source    = source.replaceAll( '{' + name + '}', value );
        }

        return source;
    }

    /** 將來源字串中所有的字串取代掉。
     * @description 將字串中所有oldString參數的字串，用newString參數值給取代掉。
     * @example let r = 'aa-bb-cc-dd'.replaceAll( '-', '_' );   // aa_bb_cc_dd
     * @param {String} oldString 要被取代掉的舊字串。
     * @param {String} newString 取代掉舊字串的新字串值。
     * @returns {String} 取代後的字串值。
     */
    String.prototype.replaceAll = function ( oldString, newString ) {
        return this.replace( new RegExp( oldString, "gm" ), newString );
    };

    /** 判斷字串是否包含指定的字串
     * @description 檢查字串中是否有包含指定的字串，效果應該等同 ECMAScript 6 提供的 includes( searchString[, position] ) 函式。
     * @example 
     * let result = 'some_text_for_test'.contains( 'for' ); // true
     * let result = 'some_text_for_test'.contains('error'); // false
     * @param {String} target 目標字串
     * @returns {Boolean} 是否包含指定的字串
     */
    String.prototype.contains = function ( target ) {
        return this.indexOf( target ) >= 0;
    };

    /** 取得字串的byte長度
     * @description 取得字串的byte長度，字串的位元長度中文字長度為 2，英數長度為 1。
     * @example
     * let str = '我是puma';
     * str.length;      // 中文字(2), 英文字(4), 總共(6)
     * str.byteLength;  // 中文字(4), 英文字(4), 總共(8)
     * @returns {Number} 字串byte的長度。
     */
    String.prototype.byteLength = function () {
        let arr = this.match( /[^\x00-\xff]/ig );
        return arr === null ? this.length : this.length + arr.length;
    };
    
    /** 去除掉字串中前後所有的空白字元
     * @description 去除字串中前後所有空格。
     * @returns {String} 去頭去尾後的字串。 
     */
    String.prototype.trim = function () {
        return this.replace( /^\s+|\s+$/g, '' );
    };
    
    /** 將字串轉換為Boolean值。
     * @description 將Boolean字串轉換成真正的JavaScript的Boolean值 (可以忽略字串的大小寫)，只有在字串值為true的情況，才會轉換成布林true值，其他一率轉換成布林false值。
     * @returns {Boolean} Boolean值。
     */
    String.prototype.toBoolean = function () {
        return "true" === this.toLowerCase();
    };

    /** 對字串進行格式化
     * @description 依照字串中指定的取代字符，進行字串的格式化。
     * @example
     * let result = 'My name is {user name}. I am {age}. {Hello World}'.format( { 'user name': 'Pony Lin', 'age': 31, 'Hello World': 'How are you doing?' } );
     * console.log( result );   // My name is Pony Lin. I am 31. How are you doing?
     * let r = 'http://localhost:2579/api/ib/account/{account}/order/'.format( { account : 'DU333231' } );
     * console.log( r );        // http://localhost:2579/api/ib/account/DU333231/order/
     * @param {Object} parameters 格式化參數集合
     * @returns {String} 格式化後的字串。
     */
    String.prototype.format = function ( parameters ) {
        return stringFormat( this.toString(), parameters );
    };

    /** 對來源字串進行格式化
     * @description 依照字串中指定的取代字符，進行字串的格式化。
     * @example
     * let result = String.format( 'My name is {user name}. I am {age}. {Hello World}', { 'user name': 'Pony Lin', 'age': 31, 'Hello World': 'How are you doing?' } );
     * console.log( result );   // My name is Pony Lin. I am 31. How are you doing?
     * let r = String.format( 'http://localhost:2579/api/ib/account/{account}/order/', { account : 'DU333231' } );
     * console.log( r );        // http://localhost:2579/api/ib/account/DU333231/order/
     * @param {String} source 來源字串
     * @param {Object} parameters 格式化參數集合
     * @returns {String} 格式化後的字串。
     */
    String.format = function ( source, parameters ) {
        return stringFormat( source, parameters );
    };

    /** 移除字串中第一個出現的「目標字串」
     * @description 移除字串中第一個出現的「目標字串」。
     * @param {String} target 目標移除字串
     * @returns {String} 移除後的結果字串。
     */
    String.prototype.removeFirstAppeared = function ( target ) {
        if ( !this.contains( target ) ) {
            return this;
        }

        return this.replace( target, '' );
    };
}() );