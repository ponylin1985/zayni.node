const readLine   = require( 'readline' );
const colors     = require( 'colors' );
const zayni      = require( './common.js' );
const extentions = require( './extensions.js' );
const jsonUtil   = require( './json.js' );
const logger     = require( './log.js' );
const dao        = require( './dataAccess/mysql-dao.js' );
const common     = zayni.common;
const guid       = zayni.guid;

let cfg = {
    server	 : 'localhost',
    uid	     : 'pony',
    password : 'Admin123',
    port	 : 3306,
    database : 'mylab'
};

( async () => {
    let db = await dao.getConnection( cfg );
    let r  = await dao.execute( db, `SELECT * FROM user;` );

    let models = r.result;

    for ( let model of models ) {
        console.log( `UserId: ${model.UserId}, Name: ${model.Name}, Age: ${model.Age}, Sex: ${model.Sex}, CreateTime: ${model.CreateTime}.` );
        console.log();
    }

    let json = JSON.stringify( models, null, 4 );
    console.log( json );
} )();

console.log( 'good' );